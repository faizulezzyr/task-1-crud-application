

import Foundation
import UIKit
import Alamofire
import NotificationCenter

class APICommunicator: NSObject {
    
    var view: UIView?
    var delegate: APICommunicatorDelegate?
    var sessionManager: Alamofire.SessionManager?
    
    init(view: UIView) {
        self.view = view       
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(30)
        configuration.timeoutIntervalForResource = TimeInterval(30)
        configuration.httpAdditionalHeaders?.updateValue("application/json", forKey: "Content-Type")
        configuration.httpAdditionalHeaders?.updateValue("application/json", forKey: "Accept")
        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
       func getDataByGET(_ url: String, methodTag: Int, withHeader: Bool) {
        
        let text = "Please wait..."
        SwiftOverlays.showCenteredWaitOverlayWithText(self.view!, text: text)
        self.view?.isUserInteractionEnabled = false
        
        if Reachability.isConnectedToNetwork() == true {
            
            if !withHeader {
                self.sessionManager?.request(url, method: .get).responseJSON { response in
                    switch response.result {
                    case .success:
                        if let statusCode = response.response?.statusCode {
                            if statusCode == Constants.SUCCESS_STATUS_CODE || statusCode == Constants.CREATED_STATUS_CODE || statusCode == Constants.FAILED_STATUS_CODE || statusCode == Constants.ERROR_STATUS_CODE {
                                if let responseObject = response.result.value {
                                    self.view?.isUserInteractionEnabled = true
                                    if let result = responseObject as? Dictionary<String, Any> {
                                        SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                        self.delegate?.taskCompletationHandler(methodTag, data: result, statusCode: statusCode)
                                    }
                                    else{
                                        SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                        CustomAlertView.customAlertShow("", msgBody: "No data found!!!", delegate: self)
                                    }
                                }
                                else{
                                    self.view?.isUserInteractionEnabled = true
                                    SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                }
                            }
                        }
                    case .failure(let error):
                        self.view?.isUserInteractionEnabled = true
                        if error._code == NSURLErrorTimedOut {
                            
                            SwiftOverlays.removeAllOverlaysFromView(self.view!)
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverError"), object: nil)
                        }
                        else {
                            
                            SwiftOverlays.removeAllOverlaysFromView(self.view!)
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverError"), object: nil)
                        }
                    }
                }
            }
            else {
                self.sessionManager?.request(url, method: .get, headers: APIHelpers.getAuthHeader()).responseJSON { response in
                    switch response.result {
                    case .success:
                        if let statusCode = response.response?.statusCode {
                            if statusCode == Constants.SUCCESS_STATUS_CODE || statusCode == Constants.CREATED_STATUS_CODE || statusCode == Constants.FAILED_STATUS_CODE ||  statusCode == Constants.UNAUTHORIZE_AGENT || statusCode == Constants.ERROR_STATUS_CODE  {
                                if let responseObject = response.result.value {
                                    self.view?.isUserInteractionEnabled = true
                                    if let result = responseObject as? Dictionary<String, Any> {
                                        SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                        self.delegate?.taskCompletationHandler(methodTag, data: result, statusCode: statusCode)
                                    }
                                    else{
                                        SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                        CustomAlertView.customAlertShow("", msgBody: "No data found!!!", delegate: self)
                                    }
                                }
                                else{
                                    self.view?.isUserInteractionEnabled = true
                                    SwiftOverlays.removeAllOverlaysFromView(self.view!)
                                }
                            }
                        }
                    case .failure(let error):
                        self.view?.isUserInteractionEnabled = true
                        if error._code == NSURLErrorTimedOut {
                            
                            SwiftOverlays.removeAllOverlaysFromView(self.view!)
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverError"), object: nil)
                        }
                        else {
                            
                            SwiftOverlays.removeAllOverlaysFromView(self.view!)
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "serverError"), object: nil)
                        }
                    }
                }
            }
        }
        else{
            
            SwiftOverlays.removeAllOverlaysFromView(self.view!)
            self.view?.isUserInteractionEnabled = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noInternet"), object: nil)
        }
    }
}













