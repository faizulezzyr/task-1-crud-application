//
//  FilterCell.swift
//  TaskOne
//
//  Created by Innovadeaus on 12/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var salary: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
