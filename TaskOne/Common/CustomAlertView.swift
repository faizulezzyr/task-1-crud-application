

import Foundation
import UIKit

open class CustomAlertView {
    
    static func customAlertShow(_ msgtitle: String, msgBody: String, delegate: AnyObject?) {
        
        let alert: UIAlertView = UIAlertView()
        alert.title = msgtitle as String
        alert.message = msgBody as String
        alert.delegate = delegate
        alert.addButton(withTitle: "OK")
        alert.show()
    }
}




