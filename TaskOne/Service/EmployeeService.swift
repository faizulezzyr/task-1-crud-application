//
//  EmployeeService.swift
//  TaskOne
//
//  Created by Innovadeaus on 11/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit


class EmployeeService: NSObject {

var apiCommunicatorHelper: APICommunicator?
var view: UIView?

  init(_ view: UIView, communicator: APICommunicator) {
    self.view = view
    self.apiCommunicatorHelper = communicator
   }
    
    func getEmployeeList(){
        let url = Urls.BASE_URL + Urls.EMPLOYEE_LIST
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: false)
    }
    

}
