

import Foundation

protocol APICommunicatorDelegate : class {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int)

}
