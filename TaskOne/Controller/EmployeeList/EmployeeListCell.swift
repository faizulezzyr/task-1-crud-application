//
//  EmployeeListCell.swift
//  TaskOne
//
//  Created by Innovadeaus on 11/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class EmployeeListCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
