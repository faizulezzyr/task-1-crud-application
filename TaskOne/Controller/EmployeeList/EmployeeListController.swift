//
//  EmployeeListController.swift
//  TaskOne
//
//  Created by Innovadeaus on 11/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON
var employeList: [JSON] = []
var employeeIndex = 0
class EmployeeListController: UIViewController {
    var apiCommunicatorHelper: APICommunicator?
    var employeeService: EmployeeService?
    let alart = SweetAlert()
    var EmployeeInfoInit: JSON = [:]
   
    @IBOutlet weak var tableView: UITableView!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            initView()
            initApiCommunicatorHelper()
            employeeService!.getEmployeeList()
            showData()
        }
        func initApiCommunicatorHelper() {
            apiCommunicatorHelper = APICommunicator(view: self.view)
            apiCommunicatorHelper?.delegate = self
            employeeService = EmployeeService(self.view, communicator: apiCommunicatorHelper!)
        }
    
    func initView(){
         let searchController = UISearchController(searchResultsController: nil)
//           searchController.searchResultsUpdater = self.viewModel
           searchController.obscuresBackgroundDuringPresentation = false
           searchController.searchBar.placeholder = "Search Employee"
           self.navigationItem.searchController = searchController
           self.definesPresentationContext = true
    }

    func NavigateToEmployeeDetails(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let Details = mainStoryboard.instantiateViewController(withIdentifier: "employeeDetails") as? EmployeeDetailsController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [Details!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        

    }
    func NavigateToFilterController(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let Details = mainStoryboard.instantiateViewController(withIdentifier: "filterEmployee") as? EmployeeListByFilterController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [Details!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        

    }
    func showData(){
        let employInfo = Helpers.getStringValueForKey(Constants.EmployeeList)
        EmployeeInfoInit = JSON.init(parseJSON: employInfo)
        if let data = EmployeeInfoInit["data"].array{
            employeList.removeAll()
            employeList = data
            tableView.reloadData()
        }
    }
 
    
    @IBAction func filter(_ sender: Any) {
        NavigateToFilterController()
    }
    
    @IBAction func addEmployee(_ sender: Any) {
        AddEmployee()
    }
    
    ///API Response
        func getEmployeeResponse(_ data: [String: Any], statusCode: Int){
              if statusCode == Constants.STATUS_CODE_SUCCESS {
                     let response = JSON(data)
                     print(response)
                     if !response.isEmpty {
              
                        if let data = response["data"].array{
                            employeList = data
                            self.tableView.reloadData()
                        }
                        if let employeeListInfo = response.rawString(){
                            Helpers.removeValue(Constants.EmployeeList)
                            Helpers.setStringValueWithKey(employeeListInfo, key: Constants.EmployeeList)
                        }
                  }
              }
          }

    }

extension EmployeeListController : UITextFieldDelegate{
    func AddEmployee() {
         
         let alertController = UIAlertController(title: "", message: "Add Employee", preferredStyle: .alert)
         
         let saveAction = UIAlertAction(title: "Add", style: .default, handler: {
             alert -> Void in
             
             let name = alertController.textFields![0] as UITextField
             let age = alertController.textFields![1] as UITextField
             let salary = alertController.textFields![2] as UITextField
            
             
             //print("firstName \(String(describing: eventNameTextField.text)), secondName \(String(describing: descriptionTextField.text))")
             
             if name.text != "" || age.text != ""{

                self.alart.showAlert("success", subTitle: "", style: .success, buttonTitle: "Done") { (true) in

                   
                }
             }
             
         })
         
         let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
             (action : UIAlertAction!) -> Void in
         })
         
         alertController.addTextField { (textField : UITextField!) -> Void in
             textField.placeholder = "Employee Name"
             let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
             textField.addConstraint(heightConstraint)
             
         }
         alertController.addTextField { (textField : UITextField!) -> Void in
             textField.placeholder = "Age"
             let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height , relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
             textField.addConstraint(heightConstraint)
             
         }
         alertController.addTextField { (textField : UITextField!) -> Void in
             textField.placeholder = "Salary"
             let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height , relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
             textField.addConstraint(heightConstraint)
             
         }
         
         
         alertController.addAction(cancelAction)
         alertController.addAction(saveAction)
         
         self.present(alertController, animated: true, completion: nil)
     }
    func EditEmployeeData(Name: String, Age : String, Salary : String) {
            
            let alertController = UIAlertController(title: "", message: "Edit Employee", preferredStyle: .alert)
            
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
                alert -> Void in
                
                let name = alertController.textFields![0] as UITextField
                let age = alertController.textFields![1] as UITextField
                let salary = alertController.textFields![2] as UITextField

                if name.text != "" || age.text != ""{
                    self.alart.showAlert("success", subTitle: "", style: .success, buttonTitle: "Done") { (true) in

                                   
                    }
                }
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Employee Name"
                textField.text = Name
                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                textField.addConstraint(heightConstraint)
                
            }
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Age"
                textField.text = Age
                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height , relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                textField.addConstraint(heightConstraint)
                
            }
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Salary"
                textField.text = Salary
                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height , relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                textField.addConstraint(heightConstraint)
                
            }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(saveAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
}

extension EmployeeListController: UISearchResultsUpdating {

func updateSearchResults(for searchController: UISearchController) {
    print("Searching with: " + (searchController.searchBar.text ?? ""))
    let searchText = (searchController.searchBar.text ?? "")
   // self.currentSearchText = searchText
    //search()
    }
}

extension EmployeeListController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return employeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? EmployeeListCell
        if let employee_name = employeList[indexPath.row]["employee_name"].string{
            cell?.name.text = employee_name
        }
        if let employee_age = employeList[indexPath.row]["employee_age"].string{
            cell?.age.text = employee_age
        }
        if let employee_salary = employeList[indexPath.row]["employee_salary"].string{
            cell?.salary.text = "\(employee_salary) Tk"
        }
        cell?.editButton.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(EmployeeListController.callButtonPressed(_:)))
        cell?.editButton.isUserInteractionEnabled = true
        cell?.editButton.addGestureRecognizer(tap)
        cell?.cellView.setCardView()
        
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        employeeIndex = indexPath.row
        NavigateToEmployeeDetails()
        
    }
    
    @objc func callButtonPressed(_ sender: UITapGestureRecognizer) {
        if let callButtonLabel = sender.view as? UIButton {
            if let employee_name = employeList[callButtonLabel.tag]["employee_name"].string{
                if let employee_age = employeList[callButtonLabel.tag]["employee_age"].string{
                    if let employee_salary = employeList[callButtonLabel.tag]["employee_salary"].string{
                         EditEmployeeData(Name: employee_name, Age: employee_age, Salary: employee_salary)
                    }
                }
            }
           
        }
     
    }
    
}
     
    extension EmployeeListController: APICommunicatorDelegate {
        func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
            if methodTag ==  MethodTags.FIRST{
                getEmployeeResponse(data, statusCode: statusCode)
            }
        }
    }

