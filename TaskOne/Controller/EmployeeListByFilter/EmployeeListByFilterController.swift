//
//  EmployeeListByFilterController.swift
//  TaskOne
//
//  Created by Innovadeaus on 12/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmployeeListByFilterController: UIViewController {
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var filterdata =  ["<300000",">100000","<100000",]
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var emplyeFilterList:[JSON] = []
    var isFilter : Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
       isFilter = false
        finalData(compareValue: 1000000, Tag: 0)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
      }
    func navigateToEmployeeList(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          let Details = mainStoryboard.instantiateViewController(withIdentifier: "employeeList") as? EmployeeListController
          let navigationController = UINavigationController()
          navigationController.viewControllers = [Details!]
          UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    func createToolbar()
     {
         picker = UIPickerView.init()
         picker.delegate = self
         picker.backgroundColor = UIColor.white
         picker.setValue(UIColor.black, forKey: "textColor")
         picker.autoresizingMask = .flexibleWidth
         picker.contentMode = .center
         picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
         self.view.addSubview(picker)

         toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
         toolBar.barStyle = .blackTranslucent
         toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
         self.view.addSubview(toolBar)

     }
     
     @objc func onDoneButtonTapped() {
         toolBar.removeFromSuperview()
         picker.removeFromSuperview()
     }
    
    func finalData(compareValue: Int, Tag: Int){
        emplyeFilterList.removeAll()
        for index in 0..<employeList.count{
            if let salary = employeList[index]["employee_salary"].string{
                if Int(salary)! < compareValue && Tag == 0 {
                    emplyeFilterList.append(employeList[index])
                }else if Int(salary)! > compareValue  && Tag == 1{
                    emplyeFilterList.append(employeList[index])
                }
            }
        }
    }
    
    func filterData(BaseData: String){
        if BaseData == "<300000" {
            finalData(compareValue: 300000, Tag: 0)
            
        }else if BaseData == ">100000"{
            finalData(compareValue: 100000, Tag: 1)
        }else if BaseData == "<100000" {
            finalData(compareValue: 100000, Tag: 0)
        }else{
            
        }
    }

    @IBAction func back(_ sender: Any) {
        navigateToEmployeeList()
    }
    
    @IBAction func filter(_ sender: Any) {
       createToolbar()
    }
}

extension EmployeeListByFilterController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return emplyeFilterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FilterCell
        if let employee_name = emplyeFilterList[indexPath.row]["employee_name"].string{
            cell?.name.text = employee_name
        }

        if let employee_salary = emplyeFilterList[indexPath.row]["employee_salary"].string{
            cell?.salary.text = "\(employee_salary) Tk"
        }
        
        return cell!
    }
    
    
}
extension EmployeeListByFilterController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        filterdata.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       filterData(BaseData: filterdata[row])
        isFilter = true
        tableView.reloadData()
     
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return filterdata[row]
    }
    
    
}
