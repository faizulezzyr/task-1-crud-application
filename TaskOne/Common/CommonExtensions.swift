

import Foundation
import UIKit

enum Priority: Int {
    
    case First = 1,
    Second,
    Third,
    Fourth,
    Fifth
}

enum LoginType: Int {
    
    case AppLogin = 1,
    GoogleLogin,
    FacebookLogin,
    GuestLogin
}

enum UserType: Int {
    
    case One = 1,
    Two,
    Three,
    Four
}

extension NSDate {
    var localTime: String {
        return description(with: NSLocale.current)
    }
}
extension UITextField {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}



extension UIImage{
    func toBase64() -> String{
        let imageData = self.pngData()
        return (imageData?.base64EncodedString())!
    }
    
    
}
extension UIView {
    func setCardView(){
         self.layer.cornerRadius = 5.0
         self.layer.borderColor  =  UIColor.clear.cgColor
         self.layer.borderWidth = 3.0
         self.layer.shadowOpacity = 0.5
        self.layer.shadowColor =  UIColor.lightGray.cgColor
         self.layer.shadowRadius = 5.0
         self.layer.shadowOffset = CGSize(width:3, height: 3)
         self.layer.masksToBounds = false
    }
}

extension String {

    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func isValidEmail(address: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: address)
    }
    
    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
    
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
        
        
    }
    
}

extension Double {
    func toString() -> String {
        return String(format: "%f",self)
    }
}

extension Notification.Name {
    static let pushPayload = Notification.Name(rawValue: "pushPayload")
}

extension UIAlertController {
    
    func changeFont(view: UIView, font:UIFont) {
        for item in view.subviews {
            if item.isKind(of: UICollectionView.self) {
                let col = item as! UICollectionView
                for  row in col.subviews{
                    changeFont(view: row, font: font)
                }
            }
            if item.isKind(of: UILabel.self) {
                let label = item as! UILabel
                label.font = font
            }else {
                changeFont(view: item, font: font)
            }
        }
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let font = UIFont.systemFont(ofSize: 15)
        changeFont(view: self.view, font: font )
    }
}

extension UIButton {
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}



extension UIAlertController {
    
    func addImage(image: UIImage){
        let maxSize = CGSize(width: 138, height: 72)
        let imgsize = image.size
        
        var ratio: CGFloat!
        if (imgsize.width > imgsize.height){
            ratio = maxSize.width / imgsize.width
        }else {
            ratio = maxSize.height / imgsize.height
        }
        let scaledSize = CGSize(width: imgsize.width * ratio, height: imgsize.height * ratio)
        //let resiezeImage = image.imageWithSize(_size: scaledSize)
        let imgAction = UIAlertAction(title: "", style: .default, handler: nil)
        imgAction.isEnabled = false
//        imgAction.setValue(resiezeImage.withRenderingMode(.alwaysOriginal), forKey: "image")
        imgAction.setValue(image, forKey: "image")
        self.addAction(imgAction)
    }
    
}

extension UIImage {
    func imageWithSize(_size: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        let aspectWidth: CGFloat = size.width / self.size.width
        let aspectHeight: CGFloat = size.height / self.size.height
        let aspectRation: CGFloat = min(aspectWidth, aspectHeight)
        scaledImageRect.size.width = self.size.width * aspectRation
        scaledImageRect.size.height = self.size.height * aspectRation
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
}




