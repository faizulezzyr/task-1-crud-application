

import Foundation
import SwiftyJSON

class Helpers: NSObject {

    static func setStringValueWithKey(_ data: String, key: String) {
        
        let appData = UserDefaults.standard
        appData.set(data, forKey: key)
        appData.synchronize()
    }
    
    static func setBoolValueWithKey(_ data: Bool, key: String) {
        
        let appData = UserDefaults.standard
        appData.set(data, forKey: key)
        appData.synchronize()
    }
    
    static func setIntValueWithKey(_ data: Int, key: String) {
        
        let appData = UserDefaults.standard
        appData.set(data, forKey: key)
        appData.synchronize()
    }
    
    static func getStringValueForKey(_ key: String) -> String {
        
        let appData = UserDefaults.standard
        if let value = appData.object(forKey: key) as? String {
            return value
        }
        return ""
    }
    
    static func getBoolValueForKey(_ key: String) -> Bool {
        
        let appData = UserDefaults.standard
        if let value = appData.object(forKey: key) as? Bool {
            return value
        }
        return false
    }
    
    static func getIntValueForKey(_ key: String) -> Int {
        
        let appData = UserDefaults.standard
        if let value = appData.object(forKey: key) as? Int {
            return value
        }
        return -1
    }
    
    static func removeValue(_ key: String) {
        
        let appData = UserDefaults.standard
        appData.removeObject(forKey: key)
        appData.synchronize()
    }
}








