//
//  EmployeeDetailsController.swift
//  TaskOne
//
//  Created by Innovadeaus on 11/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class EmployeeDetailsController: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var salary: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         showdata()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
      }
    func navigateToEmployeeList(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          let Details = mainStoryboard.instantiateViewController(withIdentifier: "employeeList") as? EmployeeListController
          let navigationController = UINavigationController()
          navigationController.viewControllers = [Details!]
          UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    func showdata(){
        if let employee_name = employeList[employeeIndex]["employee_name"].string{
            self.name.text = employee_name
        }
        if let employee_age = employeList[employeeIndex]["employee_age"].string{
            self.age.text = employee_age
        }
        if let employee_salary = employeList[employeeIndex]["employee_salary"].string{
           self.salary.text = "\(employee_salary) Tk"
        }
    }
    
    @IBAction func back(_ sender: Any) {
       navigateToEmployeeList()
    }
    


}
