
import Foundation

class Constants: NSObject {
    
    // Status Code
    static var STATUS_CODE_SUCCESS = 200
    static var NO_DATA_FOUND = 204
    static var VALIDATION_ERROR = 422
    static var UNAUTHORIZE_AGENT = 401
    
    static var SUCCESS_STATUS_CODE = 200
    static var CREATED_STATUS_CODE = 201
    static var SUCCESS_IMAGE_UPLOAD_STATUS_CODE = 201
    static var FAILED_STATUS_CODE = 500
    static var ERROR_STATUS_CODE = 409
    static var DIFFRENT_ERROR_STATUS_CODE = 400
    static var NO_DATA_STAUS_CODE = 404
    
    static var STATUS_OK = 1
    static var STATUS_FAILED = 0
    static var HEADER_KEY = "Authorization"
    static var ACCESS_TOKEN = "access_token"
    static var EmployeeList = "employeeList"
    
    // Constant Keys
    
}








